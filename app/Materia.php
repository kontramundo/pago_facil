<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Calificacion;

class Materia extends Model
{
    protected $table = 't_materias';

    protected $primaryKey = 'id_t_materias';

    protected $fillable = array('nombre', 'activo');

    protected $hidden = array("activo");

    public function calificaciones() {

        return $this->hasMany(Calificacion::class, 'id_t_materias');
    }
}
