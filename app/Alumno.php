<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Calificacion;

class Alumno extends Model
{
    protected $table = 't_alumnos';

    protected $primaryKey = 'id_t_usuarios';

    protected $fillable = array('nombre', 'ap_paterno', 'ap_materno', 'activo');

    protected $hidden = array("activo");

    public function calificaciones() {

        return $this->hasMany(Calificacion::class, 'id_t_usuarios');
    }
}
