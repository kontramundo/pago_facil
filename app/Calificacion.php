<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Alumno;
use App\Materia;

class Calificacion extends Model
{
    protected $table = 't_calificaciones';

    protected $primaryKey = 'id_t_calificaciones';

    protected $fillable = array('id_t_materias', 'id_t_usuarios', 'calificacion', 'fecha_registro');

    protected $hidden = array('id_t_materias', 'id_t_usuarios');

    public $timestamps = false;

    public function alumno() {

        return $this->belongsTo(Alumno::class, 'id_t_usuarios')->select('id_t_usuarios', 'nombre', 'ap_paterno', 'ap_materno');
    }

    public function materia() {

        return $this->belongsTo(Materia::class, 'id_t_materias')->select('id_t_materias', 'nombre');
    }

    public function getPromedioAttribute(){

		return $this->avg('calificacion');
	}
}
