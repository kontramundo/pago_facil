<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Alumno;
use App\Materia;
use App\Calificacion;

class CalificacionController extends Controller
{
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $idCalificacion)
    {
        try{
            $calificacion = Calificacion::find($idCalificacion);

            if(!$calificacion || !$request->input('calificacion'))
            {
                return response()->json(['success' =>'false', 'msg' => 'Datos Erroneos o Incompletos', 'codigo' => 422], 422);
            }
            
            $calificacion->calificacion = $request->input('calificacion');

            $calificacion->save();

            return response()->json(['success' =>'true', 'msg' => 'Calificación actualizada', 'codigo' => 201], 201);
        }
        catch(Exception $e)
        {
            return response()->json(['success' =>'false', 'msg' => 'Error Al Actualizar Calificación', 'codigo' => 500], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($idCalificacion)
    {
        try{
            $calificacion = Calificacion::find($idCalificacion);

            if(!$calificacion)
            {
                return response()->json(['success' =>'false', 'msg' => 'Datos Erroneos o Incompletos', 'codigo' => 422], 422);
            }
        

            $calificacion->delete();

            return response()->json(['success' =>'true', 'msg' => 'Calificación Eliminada', 'codigo' => 201], 201);
        }
        catch(Exception $e)
        {
            return response()->json(['success' =>'false', 'msg' => 'Error Al Eliminar Calificación', 'codigo' => 500], 500);
        }
    }
}
