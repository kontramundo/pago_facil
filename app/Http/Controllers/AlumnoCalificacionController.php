<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Alumno;
use App\Materia;
use App\Calificacion;

use Illuminate\Support\Collection;


class AlumnoCalificacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($idAlumno)
    {
        try{

            $calificaciones = Calificacion::with([
                                            'alumno' => function($q){
                                                $q->select('id_t_usuarios', 'nombre', 'ap_paterno', 'ap_materno')->where('activo', 1);
                                            },
                                            'materia' => function($q){
                                                $q->select('id_t_materias', 'nombre')->where('activo', 1);
                                            }
            ])->where('id_t_usuarios', $idAlumno)->get();

            if($calificaciones->count()<=0 || !$calificaciones[0]->alumno)
            {
                return response()->json(['success' =>'false', 'msg' => 'No se encuentra el alumno', 'codigo' => 404], 404);
            }
      
            $alumno[]  = array(
                'id_t_usuarios' => $calificaciones[0]->alumno->id_t_usuarios,
                'nombre' => $calificaciones[0]->alumno->nombre,
                'apellido' => $calificaciones[0]->alumno->ap_paterno.' '.$calificaciones[0]->alumno->ap_materno
            );

            foreach($calificaciones AS $row)
            {
                if($row->materia)
                {
                    $data[] = array(
                        'materia' => $row->materia->nombre,
                        'calificacion' => $row->calificacion,
                        'fecha' => date('d-m-Y', strtotime($row->fecha_registro)),
                        );
                }
            }

            
            return response()->json([
                                    'success' => 'true', 
                                    'alumno' => $alumno,
                                    'calificaciones' => $data, 
                                    'promedio' => number_format($calificaciones[0]->promedio, 2)
                                ], 200);
        }
        catch(Exception $e)
        {
            return response()->json(['success' =>'false', 'msg' => 'Error Desconocido', 'codigo' => 500], 500);
        }
        
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $idAlumno)
    {
        try{
            if(!$idAlumno|| !$request->input('id_t_materias') || !$request->input('calificacion'))
            {
                return response()->json(['success' =>'false', 'msg' => 'Datos erroneos o incompletos', 'codigo' => 422], 422);
            }
            
            Calificacion::create($request->all());

            return response()->json(['success' =>'true', 'msg' => 'Calificación Registrada', 'codigo' => 201], 201);
        }
        catch(Exception $e)
        {
            return response()->json(['success' =>'false', 'msg' => 'Error Al Registrar Calificación', 'codigo' => 500], 500);
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($idAlumno, $idCalificacion)
    {
        try{

            $calificaciones = Calificacion::with([
                                            'alumno' => function($q){
                                                $q->select('id_t_usuarios', 'nombre', 'ap_paterno', 'ap_materno')->where('activo', 1);
                                            },
                                            'materia' => function($q){
                                                $q->select('id_t_materias', 'nombre')->where('activo', 1);
                                            }
            ])
            ->where('id_t_calificaciones', $idCalificacion)
            ->where('id_t_usuarios', $idAlumno)
            ->first();

            if($calificaciones->count()<=0 || !$calificaciones->alumno)
            {
                return response()->json(['success' =>'false', 'msg' => 'No se encuentra el alumno', 'codigo' => 404], 404);
            }
      
            $alumno[]  = array(
                'id_t_usuarios' => $calificaciones->alumno->id_t_usuarios,
                'nombre' => $calificaciones->alumno->nombre,
                'apellido' => $calificaciones->alumno->ap_paterno.' '.$calificaciones->alumno->ap_materno
            );

            $data[] = array(
                'materia' => $calificaciones->materia->nombre,
                'calificacion' => $calificaciones->calificacion,
                'fecha' => date('d-m-Y', strtotime($calificaciones->fecha_registro)),
            );
     

            
            return response()->json([
                                    'success' => 'true', 
                                    'alumno' => $alumno,
                                    'calificaciones' => $data, 
                                    'promedio' => number_format($calificaciones->promedio, 2)
                                ], 200);
        }
        catch(Exception $e)
        {
            return response()->json(['success' =>'false', 'msg' => 'Error Desconocido', 'codigo' => 404], 404);
        }
    }
}
