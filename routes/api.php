<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@signup');
  


/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/

Route::group(['middleware' => 'auth:api'], function() {
    Route::resource('alumnos', 'AlumnoController', ['only' => ['index', 'show']]);
    Route::resource('materias', 'MateriaController', ['only' => ['index', 'show']]);
    Route::resource('calificaciones', 'CalificacionController', ['only' => ['update', 'destroy']]);
    Route::resource('alumnos.calificaciones', 'AlumnoCalificacionController', ['only' => ['index', 'show', 'store']]);

    Route::get('logout', 'AuthController@logout');
    Route::get('user', 'AuthController@user');
});



